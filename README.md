# Visitor Motivation Survey for Drupal 7

This is a Drupal 7 adaptation of the VMS plug-in. It was put together for use specifically with the Yerba Buena Center for the Arts website, http://ybca.org by Chris Brown,
cbrown@ybca.org  and further modified by Andrew Fox at the Fine Arts Museums of San Francisco (afox@famsf.org). Your mileage may vary!

To use this module, your website will need to be using jQuery 1.7 or above.

## DEPENDENCIES

This module is dependent on the jQuery update module. https://www.drupal.org/project/jquery_update

## HOW TO INSTALL:

To use, add the entire visitor_motivation_survey folder this to your sites/all/modules folder and install as per usual: https://www.drupal.org/documentation/install/modules-themes/modules-7

## FILES TO EDIT:

You WILL need to edit the language in the "markup" section of the visitor_motivation_survey.module file (lines 37 to 60) to suit your institution's name and perhaps tweak the language to match the feel of your site, e.g. having the user make the selection "Nope" may not match your site's tone.

You will LIKELY need to edit the vms.css file if the default color scheme or other aspects of the default design do not work with your site or the CSS selectors have conflicts.

## NO WARRANTY:

I don't normally build sanitized modules for general distribution, so expect some hackiness. I'm happy to help if I can, but don't expect a whole lot! cbrown@ybca.org.

Enjoy!
